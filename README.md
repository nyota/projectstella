# README #

Website name: localhost/myproject/

All CSS are inside css folder

Home page name is: index.htmlv
    Used viewport, bootstrap, and media querly in different mode of portrait and 
    landscape

    CSS filename is: style.css

Landing page name: registrationForm.html
    Used viewport and media query in different mode of portrait and 
    landscape

     CSS filename is: style_register.css

Contact page name: contact.html
    Used viewport and Media Query in different mode of portrait and 
    landscape

# MEDIAQUERY #
/* 
   ##Device = Tablets, Ipads (portrait)
*/

@media (min-width: 768px) and (max-width: 1024px)  and (orientation: portrait) {   
}

/* 
  ##Device = Tablets, Ipads (landscape)
*/

@media (min-width: 768px) and (max-width: 1024px) and (orientation: landscape) { 
}


/* 
##Device =  Most of the Smartphones Mobiles (Portrait, Landscape) 
*/

@media (min-width: 320px) and (max-width: 480px) and (orientation: portrait) {   
}


/* 
  ##Device =  Mobiles (Portrait, Landscape) 
*/
@media (min-width: 481px) and (max-width: 767px)  and (orientation: landscape){
  
}

Login Page is: login.html
Used #mobile first:  

# Mobile first #
.col-xs-	>>>>> Extra Small	Phones Less than 768px

.col-sm-	>>>>> Small Devices	Tablets 768px and Up (mobile first)

.col-md-	>>>>> Medium Devices	Desktops 992px and Up

.col-lg-	>>>>> Large Devices	Large Desktops 1200px and Up

